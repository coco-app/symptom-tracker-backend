# Coco - Your Quarantine Companion

This is the backend of a prototype implemented during the EUvsVirus hackathon (24–26 Apr 2020).
_Coco - Your Quarantine Companion_ reduces the manual communication burden between health authorities and people infected or exposed to COVID-19 who need to be in quarantine.

## Development

### Prerequities
* JDK
* PostgreSQL database

### Local Development
* checkout the repo
* configure database connection in /src/main/resources/application.properties
* run `./mvnw clean spring-boot:run`
