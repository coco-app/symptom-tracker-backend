package de.geekhub.symptomtrackerbackend.mapper;


import de.geekhub.symptomtrackerbackend.bo.HealthAuthorityBo;
import de.geekhub.symptomtrackerbackend.bo.PatientBo;
import de.geekhub.symptomtrackerbackend.bo.StatusBo;
import de.geekhub.symptomtrackerbackend.dto.HealthAuthorityFullDto;
import de.geekhub.symptomtrackerbackend.dto.HealthAuthorityNameDto;
import de.geekhub.symptomtrackerbackend.dto.HealthAuthorityNameAndCodeDto;
import de.geekhub.symptomtrackerbackend.dto.PatientRequestDto;
import de.geekhub.symptomtrackerbackend.dto.PatientResponseDto;
import de.geekhub.symptomtrackerbackend.dto.PatientStatusDto;
import de.geekhub.symptomtrackerbackend.dto.StatusDto;


public class DtoMapper {

    public static HealthAuthorityBo mapHealthAuthority( final HealthAuthorityNameDto dto ) {
        HealthAuthorityBo bo = new HealthAuthorityBo();
        bo.setName( dto.getName() );

        return bo;
    }

    public static HealthAuthorityFullDto mapHealthAuthority( final HealthAuthorityBo bo ) {
        HealthAuthorityFullDto dto = new HealthAuthorityFullDto();
        dto.setId( bo.getId() );
        dto.setName( bo.getName() );
        dto.setCode( bo.getCode() );

        return dto;
    }

    public static HealthAuthorityNameDto mapHealthAuthorityName( final HealthAuthorityBo bo ) {
        HealthAuthorityNameDto dto = new HealthAuthorityNameDto();
        dto.setName( bo.getName() );

        return dto;
    }

    public static PatientBo mapPatient( final PatientRequestDto dto ) {
        PatientBo bo = new PatientBo();
        bo.setName( dto.getName() );

        return bo;
    }

    public static PatientResponseDto mapPatient( final PatientBo patientBo, final HealthAuthorityBo healthAuthorityBo ) {
        PatientResponseDto dto = new PatientResponseDto();
        dto.setId( patientBo.getId() );
        dto.setName( patientBo.getName() );
        dto.setHealthAuthority( mapResponseHealthAuthority( healthAuthorityBo ) );

        return dto;
    }

    private static HealthAuthorityNameAndCodeDto mapResponseHealthAuthority( final HealthAuthorityBo bo ) {
        HealthAuthorityNameAndCodeDto dto = new HealthAuthorityNameAndCodeDto();
        dto.setName( bo.getName() );
        dto.setCode( bo.getCode() );

        return dto;
    }

    public static StatusBo mapStatus( final StatusDto dto ) {
        StatusBo bo = new StatusBo();
        bo.setInfectedSince( dto.getInfectedSince() );
        bo.setSymptomFreeSince( dto.getSymptomFreeSince() );

        return bo;
    }

    public static PatientStatusDto mapPatientStatus( final PatientBo patient, final StatusBo status ) {
        PatientStatusDto dto = new PatientStatusDto();
        dto.setName( patient.getName() );
        dto.setInfectedSince( status.getInfectedSince() );
        dto.setSymptomFreeSince( status.getSymptomFreeSince() );

        return dto;
    }
}
