package de.geekhub.symptomtrackerbackend;


import static de.geekhub.symptomtrackerbackend.mapper.DtoMapper.mapHealthAuthority;
import static de.geekhub.symptomtrackerbackend.mapper.DtoMapper.mapHealthAuthorityName;
import static de.geekhub.symptomtrackerbackend.mapper.DtoMapper.mapPatient;
import static de.geekhub.symptomtrackerbackend.mapper.DtoMapper.mapPatientStatus;
import static de.geekhub.symptomtrackerbackend.mapper.DtoMapper.mapStatus;
import static java.lang.String.format;
import static java.util.Objects.isNull;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import de.geekhub.symptomtrackerbackend.bo.HealthAuthorityBo;
import de.geekhub.symptomtrackerbackend.bo.PatientBo;
import de.geekhub.symptomtrackerbackend.bo.StatusBo;
import de.geekhub.symptomtrackerbackend.dto.HealthAuthorityFullDto;
import de.geekhub.symptomtrackerbackend.dto.HealthAuthorityNameDto;
import de.geekhub.symptomtrackerbackend.dto.PatientRequestDto;
import de.geekhub.symptomtrackerbackend.dto.PatientResponseDto;
import de.geekhub.symptomtrackerbackend.dto.PatientStatusDto;
import de.geekhub.symptomtrackerbackend.dto.StatusDto;
import de.geekhub.symptomtrackerbackend.exception.HealthAuthorityNotFoundException;
import de.geekhub.symptomtrackerbackend.persistence.HealthAuthorityRepository;
import de.geekhub.symptomtrackerbackend.persistence.PatientRepository;
import de.geekhub.symptomtrackerbackend.persistence.StatusRepository;


@RestController
public class Controller {

    private final HealthAuthorityRepository healthAuthorityRepository;
    private final PatientRepository patientRepository;
    private final StatusRepository statusRepository;

    Controller( HealthAuthorityRepository healthAuthorityRepository,
                PatientRepository patientRepository,
                StatusRepository statusRepository ) {
        this.healthAuthorityRepository = healthAuthorityRepository;
        this.patientRepository = patientRepository;
        this.statusRepository = statusRepository;
    }

    @GetMapping( "/" )
    public String helloWorld() {
        return "Hello World";
    }

    @GetMapping( "/health-authority/{code}" )
    public HealthAuthorityNameDto getHealthAuthority( @PathVariable String code ) {
        HealthAuthorityBo bo = healthAuthorityRepository.findByCode( code );

        if( isNull( bo ) ) {
            throw new HealthAuthorityNotFoundException();
        }

        return mapHealthAuthorityName( healthAuthorityRepository.findByCode( code ) );
    }

    @PostMapping( "/health-authority" )
    public HealthAuthorityFullDto registerHealthAuthority( @RequestBody HealthAuthorityNameDto dto ) {
        HealthAuthorityBo bo = mapHealthAuthority( dto );

        bo.setId( UUID.randomUUID().toString() );
        bo.setCode( randomCode().toString() );

        healthAuthorityRepository.save( bo );

        return mapHealthAuthority( bo );
    }

    private Integer randomCode() {
        return new Random().nextInt( ( 99999 - 10000 ) + 1 ) + 10000;
    }

    @PostMapping( "/patient" )
    public PatientResponseDto registerPatient( @RequestBody PatientRequestDto dto ) {
        PatientBo bo = mapPatient( dto );

        HealthAuthorityBo healthAuthority = healthAuthorityRepository.findByCode( dto.getHealthAuthorityCode() );
        if( isNull( healthAuthority ) ) {
            throw new HealthAuthorityNotFoundException();
        }

        bo.setId( UUID.randomUUID().toString() );
        bo.setHealthAuthorityId( healthAuthority.getId() );

        patientRepository.save( bo );

        return mapPatient( bo, healthAuthority );
    }

    @PutMapping( "/patient/{patientId}" )
    public ResponseEntity updatePatientStatus( @PathVariable String patientId, @RequestBody StatusDto dto ) {
        if( !patientRepository.findById( patientId ).isPresent() ) {
            return new ResponseEntity( HttpStatus.NOT_FOUND );
        }

        StatusBo bo = mapStatus( dto );

        bo.setPatientId( patientId );

        statusRepository.save( bo );

        return new ResponseEntity( HttpStatus.NO_CONTENT );
    }

    @GetMapping( value = "/health-authority/{id}/patients" )
    public ResponseEntity getPatients( @PathVariable String id, @RequestHeader( "Accept" ) String accept ) {
        if( !healthAuthorityRepository.findById( id ).isPresent() ) {
            throw new HealthAuthorityNotFoundException();
        }

        List<PatientBo> patients = patientRepository.findAllByHealthAuthorityId( id );
        List<PatientStatusDto> patientStatus = patients.stream().map( patientBo -> {
            Optional<StatusBo> statusBo = statusRepository.findById( patientBo.getId() );
            return statusBo.map( bo -> mapPatientStatus( patientBo, bo ) ).orElseGet( () -> mapPatientStatus( patientBo, new StatusBo() ) );
        } ).collect( Collectors.toList() );

        if( "text/csv".equals( accept ) ) {
            StringBuilder output = new StringBuilder( "Name,Infected since,Symptom free since\n" );

            for( PatientStatusDto patientStatusDto : patientStatus ) {
                output.append( format( "%s,%s,%s\n",
                                       patientStatusDto.getName(),
                                       patientStatusDto.getInfectedSince(),
                                       patientStatusDto.getSymptomFreeSince() ) );
            }

            return new ResponseEntity<>( output.toString(), HttpStatus.OK );
        }

        return new ResponseEntity<>( patientStatus, HttpStatus.OK );
    }
}
