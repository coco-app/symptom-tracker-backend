package de.geekhub.symptomtrackerbackend.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;


@Data
@JsonIgnoreProperties( ignoreUnknown = true )
@JsonNaming( PropertyNamingStrategy.SnakeCaseStrategy.class )
public class PatientRequestDto {

    private String name;
    private String healthAuthorityCode;
}
