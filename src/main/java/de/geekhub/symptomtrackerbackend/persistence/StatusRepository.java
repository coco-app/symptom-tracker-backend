package de.geekhub.symptomtrackerbackend.persistence;


import org.springframework.data.jpa.repository.JpaRepository;

import de.geekhub.symptomtrackerbackend.bo.StatusBo;


public interface StatusRepository extends JpaRepository<StatusBo, String> {
}
