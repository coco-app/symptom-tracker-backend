package de.geekhub.symptomtrackerbackend.persistence;


import org.springframework.data.jpa.repository.JpaRepository;

import de.geekhub.symptomtrackerbackend.bo.HealthAuthorityBo;


public interface HealthAuthorityRepository extends JpaRepository<HealthAuthorityBo, String> {

    HealthAuthorityBo findByCode( String code );
}
