package de.geekhub.symptomtrackerbackend.persistence;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.geekhub.symptomtrackerbackend.bo.PatientBo;


public interface PatientRepository extends JpaRepository<PatientBo, String> {

    List<PatientBo> findAllByHealthAuthorityId( String id );
}
