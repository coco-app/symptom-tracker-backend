package de.geekhub.symptomtrackerbackend;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SymptomTrackerBackendApplication {

    public static void main( String[] args ) {
        SpringApplication.run( SymptomTrackerBackendApplication.class, args );
    }

}
