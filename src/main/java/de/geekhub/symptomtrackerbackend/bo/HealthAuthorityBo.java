package de.geekhub.symptomtrackerbackend.bo;


import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;


@Data
@Entity
public class HealthAuthorityBo {

    private @Id
    String id;
    private String name;
    private String code;
}
