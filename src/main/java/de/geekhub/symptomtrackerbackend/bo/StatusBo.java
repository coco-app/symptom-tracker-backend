package de.geekhub.symptomtrackerbackend.bo;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;


@Data
@Entity
public class StatusBo {

    private @Id
    String patientId;
    private Date infectedSince;
    private Date symptomFreeSince;
}
