package de.geekhub.symptomtrackerbackend.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus( value = HttpStatus.BAD_REQUEST, reason = "Health authority not found" )
public class HealthAuthorityNotFoundException extends RuntimeException {
}
